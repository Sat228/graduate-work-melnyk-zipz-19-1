$(document).ready(function(){
    $(".add-to-cart").click(function () {
        var id = $(this).attr("data-id");
        $.post("/cart/addAjax/"+id, {}, function (data) {
            $("#cart-count").html(data);
        });
        return false;
    });
});
    function sortProducts() {
    var sortSelect = document.getElementById("sortSelect");
    var selectedOption = sortSelect.options[sortSelect.selectedIndex].value;
    var currentUrl = window.location.href;

    // Отримання значень параметрів categoryId та subCategoryId з поточного URL
    var urlParts = currentUrl.split("-");
    var categoryId = urlParts[1];
    var subCategoryId = urlParts[2].split("?")[0];

    // Побудова нового URL з вибраним значенням сортування
    var url = "/catalog/category-" + categoryId + "-" + subCategoryId + "?sort=" + selectedOption;

    // Перенаправлення користувача на новий URL
    window.location.href = url;
}
