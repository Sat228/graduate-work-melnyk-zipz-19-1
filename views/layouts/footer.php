<!-- Footer -->
<style>
    .footerfixed {
        margin-top: 155px;
    }

</style>
<div id="footerfixed" class="footerfixed">
    <div  id="footerSection">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <h5>ОБЛІКОВИЙ ЗАПИС</h5>
                    <a href="/cabinet/">ВАШ ОБЛІКОВИЙ ЗАПИС</a>
                    <a href="/cabinet/edit">ОСОБИСТА ІНФОРМАЦІЯ</a>
                    <a href="/cabinet/history/">ІСТОРІЯ ЗАМОВЛЕНЬ</a>
                </div>
                <div class="span3">
                    <h5>ІНФОРМАЦІЯ</h5>
                    <a href="/contact/">КОНТАКТИ</a>
                    <a href="/register/">РЕЄСТРАЦІЯ</a>
                    <a href="/privacy/">Політика конфіденційності</a>
                </div>
                <div id="socialMedia" class="span3 pull-right">
                    <h5>СОЦ.МЕДІА </h5>
                    <a href="#"><img width="60" height="60" src="/template/themes/images/facebook.png" title="facebook" alt="facebook"/></a>
                    <a href="#"><img width="60" height="60" src="/template/themes/images/twitter.png" title="twitter" alt="twitter"/></a>
                    <a href="#"><img width="60" height="60" src="/template/themes/images/youtube.png" title="youtube" alt="youtube"/></a>
                </div>
            </div>
            <p class="pull-right" style="margin-top: 10px;">&copy; Bhdm shop</p>
        </div><!-- Container End -->
    </div>
</div>
<!-- Placed at the end of the document so the pages load faster -->
<script src="/template/themes/js/jquery.js" type="text/javascript"></script>
<script src="/template/themes/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/template/themes/js/google-code-prettify/prettify.js"></script>

<script src="/template/themes/js/jquery.lightbox-0.5.js"></script>
<script src="/template/themes/js/jquery.cycle2.js"></script>
<script src="/template/themes/js/jquery.cycle2.carousel.js"></script>
<script src="/template/themes/js/rootshop.js"></script>
<script src="/template/themes/js/cart.js"></script>
</body>
</html>
