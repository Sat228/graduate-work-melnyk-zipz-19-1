<?php
use \App\models\Category;
use \App\models\Product;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bhdm Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--Less styles -->
    <!-- Other Less css file //different less files has different color scheam
         <link rel="stylesheet/less" type="text/css" href="/template/themes/less/simplex.less">
         <link rel="stylesheet/less" type="text/css" href="/template/themes/less/classified.less">
         <link rel="stylesheet/less" type="text/css" href="/template/themes/less/amelia.less">  MOVE DOWN TO activate
    -->
    <!--<link rel="stylesheet/less" type="text/css" href="/template/themes/less/bootshop.less">
         <script src="/template/themes/js/less.js" type="text/javascript"></script> -->

    <script src="/template/themes/js/cart.js" type="text/javascript"></script>

    <!-- Bootstrap style -->
    <link id="callCss" rel="stylesheet" href="/template/themes/shop/bootstrap.min.css" media="screen"/>
    <link href="/template/themes/css/base.css" rel="stylesheet" media="screen"/>
    <!-- Bootstrap style responsive -->
    <link href="/template/themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
    <link href="/template/themes/css/font-awesome.css" rel="stylesheet" type="text/css">
    <!-- Google-code-prettify -->
    <link href="/template/themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="/template/themes/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/template/themes/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/template/themes/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/template/themes/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/template/themes/images/ico/apple-touch-icon-57-precomposed.png">
    <style type="text/css" id="enject"></style>
</head>
<body>
<!-- Header -->
<div id="header">
    <div class="container">

        <!-- Navbar -->
        <div id="logoArea" class="navbar">
            <a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-inner">
                <a class="brand" href="/"><img src="/template/themes/images/logo3.png" alt="Rootsshop"/></a>

                <ul id="topMenu" class="nav pull-right">
                    <li class="bnt"><a href="/cart/"><span class="btn btn-large ">Кошик[<span id="cart-count"><?php echo \App\components\Cart::countItems();?></span>]</span></a></li>
                    <li class="bnt"><a href="/contact"><span class="btn btn-large ">Контакт</span></a></li>
                    <li class=""><a href="/cabinet/" role="button"><span class="btn btn-large ">Кабінет</span></a></li>
                    <li class="">
                        <?php if (\App\models\User::isGuest()): ?>
                            <a href="#login" role="button" data-toggle="modal" style="padding-right:0"><span class="btn btn-large btn-success">Login</span></a>
                        <?php else: ?>
                            <a href="/logout/" role="button" style="padding-right:0"><span class="btn btn-large btn-warning">Вийти</span></a>
                        <?php endif; ?>
                        <div id="login" class="modal hide fade in" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="false" >
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3>Блок входу</h3>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal loginFrm" method="post" action="/login/">
                                    <div class="control-group">
                                        <input type="text" name="email" id="inputEmail" placeholder="Email">
                                    </div>
                                    <div class="control-group">
                                        <input type="password" name="password" id="inputPassword" placeholder="Password">
                                    </div>
                                    <div class="control-group">
                                        <label class="checkbox">
                                            <input type="checkbox"> Запам'ятай мене
                                        </label>
                                        <div class="control-group">
                                        </div>
                                    </div>
                                    <button type="submit" name="submit" class="btn btn-success">Увійти</button>
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Закрити</button>
                                </form>
                                <a href="/register" role="button" style="padding-right:0"><span class="btn btn-large btn-warning">Зареєструватися</span></a>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>

</div>
<!-- Header End -->
