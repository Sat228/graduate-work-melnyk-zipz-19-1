<?php include ROOT . '/views/layouts/header.php' ?>

<!-- Main -->
<div id="mainBody">
    <div class="container">
        <div class="row">
            <div class="span12">
                <ul class="breadcrumb">
                    <li><a href="/">Домашня сторінка</a> <span class="divider">/</span></li>
                    <li class="active">Забули пароль?</li>
                </ul>
                <h3> ЗАБУЛИ СВІЙ ПАРОЛЬ?</h3>
                <hr class="soft"/>

                <div class="row">
                    <div class="span12" style="min-height:900px">
                        <div class="well">
                            <h5>Скинути пароль</h5><br/>
                            Будь ласка, введіть адресу електронної пошти для вашого облікового запису. Вам буде надіслано код підтвердження. Отримавши код підтвердження, ви зможете вибрати новий пароль для свого облікового запису.<br/><br/><br/>
                            <?php if ($result): ?>
                                <h4>Ми надіслали новий пароль на вашу пошту! Дякую!</h4>
                            <?php else: ?>
                                <?php if (isset($errors) && is_array($errors)): ?>
                                    <ul style="color:#7c1d1d;">
                                        <?php foreach ($errors as $error): ?>
                                            <li> - <?php echo $error; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <form action="" method="post">
                                    <div class="control-group">
                                        <label class="control-label" for="inputEmail1">Адреса електронної пошти</label>
                                        <div class="controls">
                                            <input class="span3" name="email" type="text" id="inputEmail1" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <button type="submit" name="submit" class="btn block">Надіслати</button>
                                    </div>
                                </form>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


</div>
<!-- MainBody End -->

<?php include ROOT . '/views/layouts/footer.php' ?>
