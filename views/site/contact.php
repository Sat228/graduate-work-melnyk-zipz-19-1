<?php include ROOT . '/views/layouts/header.php'; ?>

<!-- Main -->
<div id="mainBody">
    <div class="container">
        <hr class="soften">
        <h1>Visit us</h1>
        <hr class="soften"/>
        <div class="row">
            <div class="span4">
                <h4>Контактна інформація</h4>
                <p>
                    info@bhdmshop.com<br/>
                    ﻿Тел. 123-456-6780<br/>
                    Факс 123-456-5679<br/>
                    веб-сайт: bhdmshop.com
                </p>
            </div>

            <div class="span4">
                <h4>Години роботи</h4>
                <h5> Понеділок – п'ятниця</h5>
                <p>09:00 - 21:00<br/><br/></p>
            </div>            <div class="span4">
                <h4>Email Us</h4>
                <?php if ($result == false) : ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <?php foreach ($errors as $error): ?>
                            <ul>
                                <li> - <?php echo $error; ?></li>
                            </ul>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <form class="form-horizontal" action="" method="post">
                        <fieldset>
                            <div class="control-group">
                                <input type="text" name="name" placeholder="Імя" class="input-xlarge"/>
                            </div>
                            <div class="control-group">
                                <input type="text" name="email" placeholder="email" class="input-xlarge"/>
                            </div>
                            <div class="control-group">
                                <input type="text" name="subject" placeholder="Прізвище"  class="input-xlarge"/>
                            </div>
                            <div class="control-group">
                                <textarea rows="3" id="textarea" name="message" class="input-xlarge"></textarea>
                            </div>
                            <input class="btn btn-large" name="submit" type="submit" value="Відправити">
                        </fieldset>
                    </form>
                <?php else: ?>
                    <p>Ваше повідомлення надіслано. Наш менеджер відповість вам пізніше</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!-- MainBody End -->

<?php include ROOT . '/views/layouts/footer.php'; ?>

