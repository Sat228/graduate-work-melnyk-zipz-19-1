<?php include ROOT . '/views/layouts/header.php'; ?>

<?php
use \App\models\Category;
use \App\models\Product;

?>

<!-- Main -->
<div id="mainBody">
    <div class="container">
        <div class="row">
            <!-- Sidebar -->
            <div id="sidebar" class="span3">
                <div class="well well-small">
                    <a id="myCart" href="/cart/">
                        <img src="/template/themes/images/ico-cart.png" alt="cart">
                        <span id="cart-count">
                            <?php echo $totalQuantity; ?>
                        </span> Товари у вашому кошику
                        <span class="badge badge-warning pull-right">
                            $<?php echo $totalPrice; ?>
                        </span>
                    </a>
                </div>
                <ul id="sideManu" class="nav nav-tabs nav-stacked">
                    <?php foreach ($categories as $category): ?>
                        <li class="subMenu<?php if ($category['id'] == $categoryId) echo 'open';?>">
                            <a href="#">
                                <?php echo $category['name']; ?>
                            </a>
                            <ul>
                                <?php if (!empty(Category::getSubCategoriesList($category['id'])) && is_array(Category::getSubCategoriesList($category['id']))): ?>
                                    <?php foreach (Category::getSubCategoriesList($category['id']) as $subCategory): ?>
                                        <li>
                                            <a href="/catalog/category-<?php echo $category['id']."-".$subCategory['id'];?>">
                                                <i class="icon-chevron-right"></i>
                                                <?php echo $subCategory['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <br/>
            </div>
            <!-- Sidebar end -->
            <div class="span9">
                <ul class="breadcrumb">
                    <li><a href="/">Головна</a> <span class="divider">/</span></li>
                    <li class="active">Назва продукції</li>
                </ul>
                <h3><?php echo Category::getSubCategoryName($categoryId, $subCategoryId); ?>
                </h3>
                <hr class="soft"/>
                <p>
                    <script>
                        function sortProducts() {
                            var sortSelect = document.getElementById("sortSelect");
                            var selectedOption = sortSelect.options[sortSelect.selectedIndex].value;
                            var currentUrl = window.location.href;

                            // Отримання значень параметрів categoryId та subCategoryId з поточного URL
                            var urlParts = currentUrl.split("-");
                            var categoryId = urlParts[1];
                            var subCategoryId = urlParts[2].split("?")[0];

                            // Побудова нового URL з вибраним значенням сортування
                            var url = "/catalog/category-" + categoryId + "-" + subCategoryId + "?sort=" + selectedOption;

                            // Перенаправлення користувача на новий URL
                            window.location.href = url;
                        }
                    </script>

                <hr class="soft"/>
                <table class="table">
                    <tr>
                        <th class="pr-4">
                            <select id="sortSelect" onchange="sortProducts()">
                                <option value="name_asc">Назва продукту A - Z</option>
                                <option value="name_desc">Назва продукту Z - A</option>
                                <option value="price_desc">Найвища ціна</option>
                                <option value="price_asc">Найнижча ціна</option>
                            </select>
                        </th>
                        <th class="pl-4 pull-right">
                            <form class="form-inline" method="GET" action="/catalog/category-<?php echo $categoryId;?>-<?php echo $subCategoryId; ?>">
<!--                                --><?php //var_dump($categoryId);?>
                                <input type="text" name="searchTerm" placeholder="Пошук"/>
                                <input type="hidden" name="subCategoryId" value="<?php echo $subCategoryId; ?>">
                                <button type="submit" class="btn btn-primary">Знайти</button>
<!--                                --><?php //var_dump($subCategoryId);?>
                            </form>
                        </th>
                    </tr>
                </table>
                <br class="clr"/>
                <!-- Content -->
                <div class="tab-content">
                    <!-- Content list -->
                    <div class="tab-pane" id="listView">
                        <?php if (isset($subCategoryProducts) && is_array($subCategoryProducts)): ?>
                            <?php foreach ($subCategoryProducts as $product): ?>
                                <?php if (!empty($_GET['searchTerm']) && stripos($product['name'], $_GET['searchTerm']) === false) continue; ?>
                                <div class="row">
                                    <div class="span2">
                                        <img class="product" src="<?php echo Product::getImage($product['id']); ?>" alt=""/>
                                    </div>
                                    <div class="span4">
                                        <h3>
                                            <?php if ($product['is_new']): ?>
                                                новий |
                                            <?php endif; ?>
                                            <?php if ($product['availability']): ?>
                                                в наявності
                                            <?php endif; ?>
                                        </h3>
                                        <hr class="soft"/>
                                        <h4><?php echo $product['name']; ?></h4>
                                        <p>
                                            <?php echo $product['title']; ?>
                                        </p>
                                        <a class="btn btn-small pull-right" href="/product/<?php echo $product['id']; ?>">Докладніше</a>
                                        <br class="clr"/>
                                    </div>
                                    <div class="span3 alignR">
                                        <form class="form-horizontal qtyFrm">
                                            <h3> $<?php echo $product['price']; ?></h3>
                                            <label class="checkbox">
                                                <input type="checkbox">  Додає продукт для порівняння
                                            </label><br/>

                                            <a href="/product/<?php echo $product['id']; ?>" class="btn btn-large btn-primary"> В кошик <i class=" icon-shopping-cart"></i></a>
                                            <a href="/product/<?php echo $product['id']; ?>" class="btn btn-large"><i class="icon-zoom-in"></i></a>

                                        </form>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p>Немає продуктів</p>
                        <?php endif; ?>
                        <hr class="soft"/>
                    </div>
                    <!-- Content list end -->
                    <!-- Content block -->
                    <div class="tab-pane active" id="blockView">
                        <ul class="thumbnails">
                            <?php
                            // Сортування товарів
                            $sortedProducts = $subCategoryProducts; // Копіюємо масив товарів
                            $sort = isset($_GET['sort']) ? $_GET['sort'] : ''; // Отримуємо значення сортування
                            switch ($sort) {
                                case 'name_asc':
                                    usort($sortedProducts, function($a, $b) {
                                        return strcmp($a['name'], $b['name']);
                                    });
                                    break;
                                case 'name_desc':
                                    usort($sortedProducts, function($a, $b) {
                                        return strcmp($b['name'], $a['name']);
                                    });
                                    break;
                                case 'price_asc':
                                    usort($sortedProducts, function($a, $b) {
                                        return $a['price'] - $b['price'];
                                    });
                                    break;
                                case 'price_desc':
                                    usort($sortedProducts, function($a, $b) {
                                        return $b['price'] - $a['price'];
                                    });
                                    break;
                                default:
                                    // Залишаємо масив товарів без змін, якщо не вибрано жодного сортування
                                    break;
                            }

                            if (!empty($_GET['searchTerm'])) {
                                // Фільтрація товарів за пошуковим терміном
                                $searchTerm = $_GET['searchTerm'];
                                $sortedProducts = array_filter($sortedProducts, function($product) use ($searchTerm) {
                                    return stripos($product['name'], $searchTerm) !== false;
                                });
                            }

                            foreach ($sortedProducts as $product) {
                                ?>
                                <li class="span3">
                                    <div class="thumbnail">
                                        <a href="/product/<?php echo $product['id']; ?>">
                                            <img class="product" src="<?php echo Product::getImage($product['id']); ?>" alt=""/>
                                        </a>
                                        <div class="caption">
                                            <h5><?php echo $product['name']; ?></h5>
                                            <h4 style="text-align:center">
                                                <a class="btn" href="/product/<?php echo $product['id']; ?>">
                                                    <i class="icon-zoom-in"></i>
                                                </a>
                                                <a class="btn add-to-cart" data-id="<?php echo $product['id']; ?>" href="#">В кошик
                                                    <i class="icon-shopping-cart"></i>
                                                </a>
                                                <a class="btn btn-primary" href="#">
                                                    $<?php echo $product['price']; ?>
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        <hr class="soft"/>
                    </div>
                    <!-- Content block end -->
                </div>
                <!-- Content end -->
                <!-- Pagination -->
                <?php echo $pagination->get(); ?>
                <!-- Pagination end -->
                <br class="clr"/>
            </div>
        </div>
    </div>
</div>

<!-- MainBody end -->
<!-- MainBody end -->
<?php include ROOT . '/views/layouts/footer.php'; ?>
