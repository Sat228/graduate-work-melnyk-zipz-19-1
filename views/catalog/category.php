<?php include ROOT . '/views/layouts/header.php'; ?>
<?php
use \App\models\Category;
use \App\models\Product;
?>
<!-- Main -->
<div id="mainBody">
    <div class="container">
        <div class="row">
            <!-- Sidebar -->
            <div id="sidebar" class="span3">
                <div class="well well-small">
                    <a id="myCart" href="/cart/">
                        <img src="/template/themes/images/ico-cart.png" alt="cart">
                        <span id="cart-count">
                            <?php echo $totalQuantity; ?>
                        </span> Товари у вашому кошику
                        <span class="badge badge-warning pull-right">
                            $<?php echo $totalPrice; ?>
                        </span>
                    </a>
                </div>
                <ul id="sideManu" class="nav nav-tabs nav-stacked">
                    <?php foreach ($categories as $category): ?>
                        <li class="subMenu<?php if ($category['id'] == $categoryId) echo ' open'; ?>">
                            <a href="#">
                                <?php echo $category['name']; ?>
                            </a>
                            <ul>
<!--                                <li><a href="/catalog/category---><?php //echo $category['id']; ?><!--">All</a></li>-->
                                <?php if (!empty(Category::getSubCategoriesList($category['id'])) && is_array(Category::getSubCategoriesList($category['id']))): ?>
                                    <?php foreach (Category::getSubCategoriesList($category['id']) as $subCategory): ?>
                                        <li>
                                            <a href="/catalog/category-<?php echo $category['id'] . "-" . $subCategory['id']; ?>">
                                                <i class="icon-chevron-right"></i>
                                                <?php echo $subCategory['name']; ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br/>
            </div>
            <!-- Sidebar end -->
            <div class="span9">
                <ul class="breadcrumb">
                    <li><a href="index.html">Головна</a> <span class="divider">/</span></li>
                    <li class="active">Назва продукції</li>
                </ul>
                <hr class="soft"/>
                <p>
                <table class="table">
                    <tr>
                        <th class="pr-4">
                            <select>
                                <option>Назва продукту A - Z</option>
                                <option>Назва продукту Z - A</option>
                                <option>Найвища ціна</option>
                                <option>Найнижча ціна</option>
                            </select>
                        </th>
                        <th class="pl-4 pull-right">
                            <form class="form-inline" method="GET" action="/catalog/category-<?php echo $categoryId; ?>">
                                <input type="text" name="searchTerm" placeholder="Пошук" value="<?php echo isset($_GET['searchTerm']) ? $_GET['searchTerm'] : ''; ?>"/>
                                <button type="submit" class="btn btn-primary">Знайти</button>
                            </form>
                        </th>
                    </tr>
                </table>
                <br class="clr"/>
                <div class="tab-content">
                    <div class="tab-pane active" id="blockView">
                        <ul class="thumbnails">
                            <?php if (!empty($categoryProducts) && is_array($categoryProducts)): ?>
                                <?php foreach ($categoryProducts as $product): ?>
                                    <?php if (!empty($_GET['searchTerm']) && stripos($product['name'], $_GET['searchTerm']) === false) continue; ?>
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <a href="/product/<?php echo $product['id']; ?>">
                                                <img class="product" src="<?php echo Product::getImage($product['id']); ?>" alt=""/>
                                            </a>
                                            <div class="caption">
                                                <h5><?php echo $product['name']; ?></h5>
                                                <h4 style="text-align:center">
                                                    <a class="btn" href="/product/<?php echo $product['id']; ?>">
                                                        <i class="icon-zoom-in"></i>
                                                    </a>
                                                    <a class="btn add-to-cart" data-id="<?php echo $product['id']; ?>" href="#">В кошик
                                                        <i class="icon-shopping-cart"></i>
                                                    </a>
                                                    <a class="btn btn-primary" href="#">
                                                        $<?php echo $product['price']; ?>
                                                    </a>
                                                </h4>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <?php endif; ?>
                        </ul>
                        <hr class="soft"/>
                    </div>
                    <!-- Content block end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MainBody End -->
<?php include ROOT . '/views/layouts/footer.php'; ?>
