<?php include ROOT . '/views/layouts/header_admin.php';
use App\models\Product;

?>

    <section>
        <div class="container">
            <div class="row">
                <br/>

                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Панель адміністратора</a></li>
                        <li><a href="/admin/product">Керуйте продуктами</a></li>
                        <li class="active">Редагувати товар</li>
                    </ol>
                </div>

                <h4>Редагувати товар</h4>

                <br/>
                <?php if(isset($errors) && (is_array($errors))): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li style="color: red;"> - <?php echo $error; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <div class="control-group">
                        <label class="control-label" for="name">Ім'я <sup>*</sup></label>
                        <div class="controls">
                            <input type="text" name="name" id="name" value="<?php echo htmlspecialchars($product['name']); ?>" placeholder="Name">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="code">Код <sup>*</sup></label>
                        <div class="controls">
                            <input type="text" name="code" id="code" value="<?php echo $product['code'];?>" placeholder="Code">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="price">Ціна <sup>*</sup></label>
                        <div class="controls">
                            <input type="text" name="price" id="input_email" value="<?php echo $product['price'];?>" placeholder="Price">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="category_id">Категорія <sup>*</sup></label>
                        <div class="controls">
                            <select name="category_id">
                                <?php if (is_array($categoriesList)): ?>
                                    <?php foreach ($categoriesList as $category): ?>
                                        <option value="<?php echo $category['id']; ?>"
                                            <?php if ($product['category_id'] == $category['id']) echo ' selected="selected"';?>>
                                            <?php echo $category['name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="subcategory_id">Підкатегорія <sup>*</sup></label>
                        <div class="controls">
                            <select name="subcategory_id">
                                <?php if (is_array($subCategoriesList)): ?>
                                    <?php foreach ($subCategoriesList as $subCategory): ?>
                                        <option value="<?php echo $subCategory['id']; ?>"
                                            <?php if ($product['sub_category_id'] == $subCategory['id']) echo ' selected="selected"'; ?>>
                                            <?php echo $subCategory['name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="brand">Brand</label>
                        <div class="controls">
                            <input type="text" name="brand" id="brand" value="<?php echo $product['brand'];?>" placeholder="Brand">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="image">Зображення</label>
                        <div class="controls">
                            <img src="<?php echo Product::getImage($product['id']); ?>" width="200" alt="" /><br/>
                            <input type="file" name="image" id="image" value="<?php echo $product['image']; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="description">Опис</label>
                        <div class="controls">
                            <textarea name="description"><?php echo $product['description'];?></textarea>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="control-group">
                        <label class="control-label" for="availability">Доступність</label>
                        <div class="controls">
                            <select id="availability" name="availability">
                                <option value="1" <?php if ($product['availability'] == 1) echo ' selected="selected"'; ?>>Yes</option>
                                <option value="0" <?php if ($product['availability'] == 0) echo ' selected="selected"'; ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="control-group">
                        <label class="control-label" for="is_new">новий</label>
                        <div class="controls">
                            <select id="is_new" name="is_new">
                                <option value="1" <?php if ($product['availability'] == 1) echo ' selected="selected"'; ?>>Yes</option>
                                <option value="0" <?php if ($product['availability'] == 0) echo ' selected="selected"'; ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="control-group">
                        <label class="control-label" for="is_featured">Рекомендовано</label>
                        <div class="controls">
                            <select id="is_featured" name="is_featured">
                                <option value="1" <?php if ($product['availability'] == 1) echo ' selected="selected"'; ?>>Yes</option>
                                <option value="0" <?php if ($product['availability'] == 0) echo ' selected="selected"'; ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="control-group">
                        <label class="control-label" for="status">Status</label>
                        <div class="controls">
                            <select id="status" name="status">
                                <option value="1" <?php if ($product['availability'] == 1) echo ' selected="selected"'; ?>>Showed</option>
                                <option value="0" <?php if ($product['availability'] == 0) echo ' selected="selected"'; ?>>Hidden</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <input type="submit" name="submit" class="btn btn-default" value="Save" />
                    <br/><br/>
                </form>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>