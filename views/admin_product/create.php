<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li><a href="/admin/product">Керуйте продуктами</a></li>
                    <li class="active">Редагувати товар</li>
                </ol>
            </div>


            <h4>Додати новий товар</h4>

            <br/>
            <?php if(isset($errors) && (is_array($errors))): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li style="color: red;"> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <div class="control-group">
                    <label class="control-label" for="name">Name <sup>*</sup></label>
                    <div class="controls">
                        <input type="text" name="name" id="name" value="" placeholder="Name">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="code">Code <sup>*</sup></label>
                    <div class="controls">
                        <input type="text" name="code" id="code" value="" placeholder="Code">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="price">Price <sup>*</sup></label>
                    <div class="controls">
                        <input type="text" name="price" id="input_email" value="" placeholder="Price">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="price">Категорія <sup>*</sup></label>
                    <div class="controls">
                        <select name="category_id">
                            <?php if (is_array($categoriesList)): ?>
                                <?php foreach ($categoriesList as $category): ?>
                                    <option value="<?php echo $category['id']; ?>">
                                        <?php echo $category['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="price">Підкатегорія <sup>*</sup></label>
                    <div class="controls">
                        <select name="subcategory_id">
                            <?php if (is_array($subCategoriesList)): ?>
                                <?php foreach ($subCategoriesList as $subCategory): ?>
                                    <?php if ($category['id'] != $subCategory['id']): ?>
                                        <option value="<?php echo $subCategory['id']; ?>">
                                            <?php echo $subCategory['name']; ?>
                                        </option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="brand">Бренд</label>
                    <div class="controls">
                        <input type="text" name="brand" id="brand" value="" placeholder="Brand">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="image">Зображення</label>
                    <div class="controls">
                        <input type="file" name="image" id="image" value="">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="image">Опис</label>
                    <div class="controls">
                        <textarea name="description"></textarea>
                    </div>
                </div>
                <br/><br/>
                <div class="control-group">
                    <label class="control-label" for="availability">Доступність</label>
                    <div class="controls">
                        <select id="availability" name="availability">
                            <option value="1" selected="selected">Так</option>
                            <option value="0">Ні</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="control-group">
                    <label class="control-label" for="is_new">New</label>
                    <div class="controls">
                        <select id="is_new" name="is_new">
                            <option value="1" selected="selected">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="control-group">
                    <label class="control-label" for="is_featured">Рекомендовано</label>
                    <div class="controls">
                        <select id="is_featured" name="is_featured">
                            <option value="1" selected="selected">Так</option>
                            <option value="0">Ні</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="control-group">
                    <label class="control-label" for="status">Статус</label>
                    <div class="controls">
                        <select id="status" name="status">
                            <option value="1" selected="selected">Showed</option>
                            <option value="0">Hidden</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <input type="submit" name="submit" class="btn btn-default" value="Save" />
                <br/><br/>
            </form>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>


