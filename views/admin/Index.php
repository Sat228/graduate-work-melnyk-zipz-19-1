<?php
use App\models\User;
use \App\models\Category;
use \App\models\Product;


include ROOT . '/views/layouts/header_admin.php';


?>

<section>
    <div class="container">
        <div class="row">
            <br/>
            <h4>Ласкаво просимо, <i style="color: #408140;"><?php echo $userName; ?></i></h4>
            <h4>Ваша роль, <i style="color: #408140;"><?php echo $role; ?></i></h4>
            <br/>
            <p>У вас є такі можливості:</p>
            <br/>
            <?php
            if ($role == 'admin') {
                echo '<div id="welcomeLine" class=" btn-group-horizontal">
                            <a class="btn  btn-lg mx-2" href="/admin/product/">Керуйте продуктами</a>
                            <a class="btn  btn-lg mx-2" href="/admin/category/">Керуйте категоріями</a>
                            <a class="btn  btn-lg mx-2" href="/admin/order/">Керуйте замовленнями</a>
                            <a class="btn  btn-lg mx-2" href="/admin/role/">Керуйте користувачами</a>
                        </div>';
            } elseif ($role == 'moder') {
                echo '<div id="welcomeLine" class=" btn-group-horizontal">
                        <a class="btn  btn-primary btn-lg mx-2" href="/admin/category/">Керуйте категоріями</a>
                        <a class="btn btn-primary btn-lg mx-2" href="/admin/product/">Керуйте продуктами</a>
                    </div>';
            }
            ?>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

