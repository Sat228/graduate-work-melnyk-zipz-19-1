<?php use App\models\User;

include ROOT . '/views/layouts/header.php'; ?>

<!-- Main -->
<div id="mainBody">
    <div class="container">
        <hr class="soften">
        <h1>Cabinet</h1>
        <hr class="soften"/>
        <div class="row">
            <div class="span12">
                <h3>Ваш Кабінет</h3>
                <div id="welcomeLine" class="">
                    <div class="">
                        <a href="/cart/">
                                <span class="btn">
                                    <i class="icon-shopping-cart icon-white"></i>
                                    [<span id="cart-count">
                                        <?php echo \App\components\Cart::countItems();?>
                                    </span>] Товари у вашому кошику
                                </span>
                        </a>
                    </div>
                </div>
                <br>
                <h4>Ласкаво просимо, <i style="color: #408140;"><?php echo $userName; ?></i></h4>
                <h4>Ваша роль, <i style="color: #408140;"><?php echo $role; ?></i></h4>
                <br>
                <?php
                    if ($role == 'admin') {
                        echo '<div id="welcomeLine" class=" btn-group-horizontal">
                            <a class="btn  btn-lg mx-2" href="/admin/product/">Керуйте продуктами</a>
                            <a class="btn  btn-lg mx-2" href="/admin/category/">Керуйте категоріями</a>
                            <a class="btn  btn-lg mx-2" href="/admin/order/">Керуйте замовленнями</a>
                            <a class="btn  btn-lg mx-2" href="/admin/role/">Керуйте користувача</a>

                        </div>';
                    } elseif ($role == 'moder') {
                        echo '<div id="welcomeLine" class=" btn-group-horizontal">
                         <a class="btn btn-primary btn-lg mx-2" href="/admin/category/">Керуйте категоріями</a>
                         <a class="btn btn-primary btn-lg mx-2" href="/admin/product/">Керуйте продуктами</a>
                        </div>';
                }
                ?>
                <h5><a class="btn  btn-lg mx-2" href="/cabinet/history">Переглянути історію замовлень</a></h5>
                <h5><a href="/cabinet/edit">Редагувати особисту інформацію</a></h5>
            </div>
        </div>
    </div>
</div>
<!-- MainBody End -->
<br><br><br><br><br><br><br><br><br><br><br><br>
<?php include ROOT . '/views/layouts/footer.php'; ?>


