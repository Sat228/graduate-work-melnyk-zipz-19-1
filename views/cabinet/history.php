<?php include ROOT . '/views/layouts/header.php'; ?>
<?php
use App\models\Order;
use App\models\User;
?>
<section>
    <div class="container">
        <div class="row">
            <br/>
            <h4>Список замовлень</h4>
            <br/>
            <h4>Ласкаво просимо, <i style="color: #408140;"><?php echo $userName; ?></i></h4>
            <h4>Ваші замовлення</i></h4>
            <table class="table-bordered table-striped table">
                <tr>
<!--                    <th>Іd</th>-->
                    <th>Ім'я</th>
                    <th>Пошта</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
                <?php foreach ($orderListById as $order): ?>
                    <tr>
<!--                        <td>--><?php //echo $order['id'];?><!--</td>-->
                        <td><?php echo $order['user_name'];?></td>
                        <td><?php echo $order['user_email'];?></td>
                        <td><?php echo $order['date'];?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                        <td><a href="/cabinet/view/<?php echo $order['id'];?>" title="View">Переглянути</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>
<br><br>
<br><br><br><br><br><br><br>
<?php include ROOT . '/views/layouts/footer.php'; ?>
