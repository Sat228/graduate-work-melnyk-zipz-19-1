<?php include ROOT . '/views/layouts/header.php'; ?>

<?php
use App\models\Order;
use App\models\Product;
?>
<section>
    <div class="container">
        <div class="row">
            <br/>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>Ім'я</th>
                    <th>Пошта</th>
                    <th>Телефон</th>
                    <th>Коментар</th>
                    <th>Дата</th>
                    <th>Статус</th>
                </tr>
                <?php if ($order): ?>
                    <tr>
                        <td><?php echo $order['user_name'];?></td>
                        <td><?php echo $order['user_email'];?></td>
                        <td><?php echo $order['user_phone'];?></td>
                        <td><?php echo $order['user_comment'];?></td>
                        <td><?php echo $order['date'];?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                    </tr>
                <?php endif; ?>
            </table>
            <br/><br/>
            <h5>Деталі замовлення:</h5>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>Назва</th>
                    <th>Код</th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['name'];?></td>
                        <td><?php echo $product['code'];?></td>
                        <td><?php echo $product['price'];?></td>
                        <td><?php echo $quantity[$product['id']];?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5" style="text-align:right"><strong>ЗАГАЛЬНА СУМА: </strong></td>
                    <td style="display:block"> <strong> $<?php echo $totalPrice; ?> </strong></td>
                </tr>
            </table>
            <h5>Товари:</h5>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>Name</th>
                    <th>фото</th>
                    <th>Price</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['name'];?></td>
                        <td><img class="product" src="<?php echo Product::getImage($product['id']); ?>" alt=""/></td>
                        <td><?php echo $product['price'];?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <br/><br/>
        </div>
    </div>
</section>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/>


<?php include ROOT . '/views/layouts/footer.php'; ?>


