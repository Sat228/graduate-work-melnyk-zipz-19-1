<?php include ROOT . '/views/layouts/header.php'; ?>

<?php
use App\components\Cart;
use App\models\Product;
use \App\models\Order;

?>

<!-- Main -->
<div id="mainBody">
    <div class="container">
        <div class="row">
            <!-- Sidebar -->
            <div id="sidebar" class="span3">

                <ul id="sideManu" class="nav nav-tabs nav-stacked">
                    <?php foreach ($categories as $category): ?>
                        <li class="<?php if ($category['id'] == 1) echo 'subMenu open'; ?>">
                            <a href="/catalog/category-<?php echo $category['id'];?>">
                                <?php echo $category['name']; ?>
                            </a>
                            <?php if ($category['id'] == 1): ?>
                                <ul>
<!--                                    <li><a href="/catalog/category---><?php //echo $category['id'];?><!--">Всі</a></li>-->
                                    <?php if (isset($subCategories) && is_array($subCategories)): ?>
                                        <?php foreach ($subCategories as $subCategory): ?>
                                            <li>
                                                <a href="/catalog/category-<?php echo $category['id']."-".$subCategory['id'];?>">
                                                    <i class="icon-chevron-right"></i>
                                                    <?php echo $subCategory['name']; ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br/>
                <div class="thumbnail">
                    <img src="/template/themes/images/payment_methods.png" title="Bootshop Payment Methods" alt="Payments Methods">
                    <div class="caption">
                        <h5>методи оплати</h5>
                    </div>
                </div>
            </div>
            <!-- Sidebar end -->

            <div class="span9">
                <ul class="breadcrumb">
                    <li><a href="/">Головна</a> <span class="divider">/</span></li>
                    <li class="active"> Кошик</li>
                </ul>
                    <h3>  Кошик [ <small><?php echo Cart::countItems();?> Товар(ів) </small>]<a href="/catalog/category-1/" class="btn btn-large pull-right"><i class="icon-arrow-left"></i> Продовжуйте покупки </a></h3>
                <hr class="soft"/>
                <?php if ($productsInCart != false): ?>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Продукт</th>
                            <th>Назва</th>
                            <th>Опис</th>
                            <th>Кількість/Оновлення</th>
                            <th>Ціна</th>
                            <th>Усього</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product): ?>
                            <tr>
                                <td> <img width="60" src="<?php echo Product::getImage($product['id']); ?>" alt=""/></td>
                                <td><br/><?php echo $product['name']; ?></td>
                                <td><br/><?php echo $product['description']; ?></td>
                                <td>
                                    <div class="input-append">
                                        <input class="span1" style="max-width:34px" placeholder="1" readonly id="appendedInputButtons" value="<?php echo $_SESSION['products'][$product['id']];?>" size="16" type="text">
                                        <?php if ($_SESSION['products'][$product['id']] > 1) { ?>
                                            <a href="/cart/delete/<?php echo $product['id'];?>">
                                                <button class="btn" type="button" >
                                                    <i class="icon-minus"></i>
                                                </button>
                                            </a>
                                        <?php } ?>
                                        <a href="/cart/add/<?php echo $product['id'];?>">
                                            <button class="btn" type="button">
                                                <i class="icon-plus"></i>
                                            </button>
                                        </a>
                                        <a href="/cart/deleteProduct/<?php echo $product['id'];?>">
                                            <button class="btn btn-danger" type="button">
                                                <i class="icon-remove icon-white"></i>
                                            </button>
                                        </a>
                                    </div>
                                </td>
                                <td>$<?php echo $product['price'];?></td>
                                <td>$<?php echo $_SESSION['products'][$product['id']] * $product['price'];?></td>
                            </tr>

                        <?php endforeach; ?>
                        <tr>
                            <td colspan="4" style="text-align:right"><strong>ЗАГАЛЬНА СУМА: </strong></td>
                            <td class="label label-important" style="display:block"> <strong > $<?php
                                    $totalPrice = 0;
                                    foreach ($products as $product) {
                                        $totalPrice += $_SESSION['products'][$product['id']] * $product['price'];
                                    }
                                    echo $totalPrice; ?> </strong></td>
                        </tr>
                        </tbody>
                    </table>
                <?php else: ?>
                    <h3>У кошику немає товарів</h3>
                <?php endif; ?>
                <a href="/catalog/category-1/" class="btn btn-large"><i class="icon-arrow-left"></i> Продовжити покупки
                </a>
                <?php if ($totalPrice >= 1) { ?>
                    <a href="/cart/checkout/" class="btn btn-large pull-right"><i class="icon-arrow-right right"></i>
                        Далі </a>
                <?php } else { ?>
                    <button class="btn btn-large pull-right" disabled><i class="icon-arrow-right right"></i> Далі
                    </button>
                    <p>Сума замовлення менше 1. Додайте товар до кошика, щоб зробити замовлення.</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- MainBody End -->
<br><br><br><br>
<?php include ROOT . '/views/layouts/footer.php'; ?>
