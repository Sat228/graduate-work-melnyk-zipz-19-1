<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li><a href="/admin/category">Керування категоріями</a></li>
                    <li class="active">Видалити підкатегорію</li>
                </ol>
            </div>

            <h4>Видалити підкатегорію # <?php echo $id; ?></h4>

            <br/>

            <p>Ви впевнені, що видалите цю підкатегорію?</p>
            <form method="post">
                <input type="submit" name="submit" value="Delete" />
            </form>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

