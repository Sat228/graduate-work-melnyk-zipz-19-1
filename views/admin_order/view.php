<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<?php
use App\models\Order;
?>

<section>
    <div class="container">
        <div class="row">
            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li><a href="/admin/order">Керуйте замовленнями</a></li>
                    <li class="active">Переглянути продукт</li>
                </ol>
            </div>

            <h4>Переглянути замовлення</h4>
            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID замовлення:</th>
                    <th>Ім'я</th>
                    <th>Пошта</th>
                    <th>Телефон</th>
                    <th>Коментар</th>
                    <th>Ідентифікатор користувача:</th>
                    <th>Дата</th>
                    <th>Продукти</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php if ($order): ?>
                    <tr>
                        <td><?php echo $order['id'];?></td>
                        <td><?php echo $order['user_name'];?></td>
                        <td><?php echo $order['user_email'];?></td>
                        <td><?php echo $order['user_phone'];?></td>
                        <td><?php echo $order['user_comment'];?></td>
                        <td><?php echo $order['user_id'];?></td>
                        <td><?php echo $order['date'];?></td>
                        <td><?php echo $order['products'];?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                        <td><a href="/admin/order/update/<?php echo $order['id'];?>" title="Edit">Редагувати</a></td>
                        <td><a href="/admin/order/delete/<?php echo $order['id'];?>" title="Edit">Видалити</a></td>
                    </tr>
                <?php endif; ?>
            </table>
            <br/><br/>
            <h5>Продукти:</h5>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID:</th>
                    <th>Назва</th>
                    <th>Код</th>
                    <th></th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['id'];?></td>
                        <td><?php echo $product['name'];?></td>
                        <td><?php echo $product['code'];?></td>
                        <td></td>
                        <td><?php echo $product['price'];?></td>
                        <td><?php echo $quantity[$product['id']];?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5" style="text-align:right"><strong>ЗАГАЛЬНА СУМА: </strong></td>
                    <td style="display:block"> <strong> $<?php echo $totalPrice; ?> </strong></td>
                </tr>
            </table>
            <br/><br/>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>


