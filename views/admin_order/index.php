<?php include ROOT . '/views/layouts/header_admin.php'; ?>
<?php
use App\models\Order;
?>
<section>
    <div class="container">
        <div class="row">
            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li class="active">Керуйте замовленнями</li>
                </ol>
            </div>

            <h4>Список замовлень</h4>
            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID замовлення:</th>
                    <th>Ім'я</th>
                    <th>Пошта</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($ordersList as $order): ?>
                    <tr>
                        <td><?php echo $order['id'];?></td>
                        <td><?php echo $order['user_name'];?></td>
                        <td><?php echo $order['user_email'];?></td>
                        <td><?php echo $order['date'];?></td>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>

                        <td><a href="/admin/order/view/<?php echo $order['id'];?>" title="View">Переглянути</a></td>
                        <td><a href="/admin/order/update/<?php echo $order['id'];?>" title="Edit">Редагувати</a></td>
                        <td><a href="/admin/order/delete/<?php echo $order['id'];?>" title="Edit">Видалити</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>
<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

