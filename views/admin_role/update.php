<?php include ROOT . '/views/layouts/header_admin.php';
use App\models\Product;
use App\models\User;

?>

    <section>
        <div class="container">
            <div class="row">
                <br/>
                <div class="breadcrumbs">
                    <ol class="breadcrumb">
                        <li><a href="/admin">Панель адміністратора</a></li>
                        <li><a href="/admin/role">Керуйте Користувачами</a></li>
                        <li class="active">Редагувати замовлення</li>
                    </ol>
                </div>

                <h4>Редагувати Користувача</h4>

                <br/>
                <?php if(isset($errors) && (is_array($errors))): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li style="color: red;"> - <?php echo $error; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
    <form class="form-horizontal" action="" method="post">
        <div class="control-group">
            <label class="control-label" for="role">Роль</label>
            <div class="controls">
                <select name="role" id="role">
                    <option value="user" <?php if ($user['role'] == 'user') echo 'selected'; ?>>user</option>
                    <option value="admin" <?php if ($user['role'] == 'admin') echo 'selected'; ?>>admin</option>
                    <option value="moder" <?php if ($user['role'] == 'moder') echo 'selected'; ?>>moder</option>
                </select>
            </div>
        </div>
        <br/><br/>
        <input type="submit" name="submit" class="btn btn-default" value="saveRole" />
        <br/><br/>
    </form>


<?php include ROOT . '/views/layouts/footer_admin.php'; ?>