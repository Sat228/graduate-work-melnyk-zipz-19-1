<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li class="active">Керуйте користувача</li>
                </ol>
            </div>
            <h4>Список користувачів</h4>
            <br/>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>Ідентифікатор:</th>
                    <th>Ім'я</th>
                    <th>Прізвище</th>
                    <th>Email</th>
                    <th>Пароль</th>
                    <th>Дата народження</th>
                    <th>Компанія</th>
                    <th>Адреса</th>
                    <th>Місто</th>
                    <th>Область</th>
                    <th>Поштовий індекс</th>
                    <th>Країна</th>
                    <th>Додаткова інформація</th>
                    <th>Телефон</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Роль</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($usersList as $user): ?>
                    <tr>
                        <td><?php echo $user['id'];?></td>
                        <td><?php echo $user['first_name'];?></td>
                        <td><?php echo $user['last_name'];?></td>
                        <td><?php echo $user['email'];?></td>
                        <td><?php echo $user['password'];?></td>
                        <td><?php echo $user['birth'];?></td>
                        <td><?php echo $user['company'];?></td>
                        <td><?php echo $user['address'];?></td>
                        <td><?php echo $user['city'];?></td>
                        <td><?php echo $user['state'];?></td>
                        <td><?php echo $user['postcode'];?></td>
                        <td><?php echo $user['country'];?></td>
                        <td><?php echo $user['additional_info'];?></td>
                        <td><?php echo $user['phone'];?></td>
                        <td><?php echo $user['date'];?></td>
                        <td><?php echo $user['status'];?></td>
                        <td><?php echo $user['role'];?></td>
                        <td><a href="/admin/role/update/<?php echo $user['id'];?>" title="Редагувати">Редагувати</a></td>
                        <td><a href="/admin/role/delete/<?php echo $user['id'];?>" title="Видалити">Видалити</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>
