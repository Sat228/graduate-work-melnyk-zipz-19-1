<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Панель адміністратора</a></li>
                    <li><a href="/admin/role">Керуйте Користувачами</a></li>
                    <li class="active">Видалити користувача</li>
                </ol>
            </div>
            <h4>Видалити користувача # <?php echo $id; ?></h4>
            <br/>
            <p>Ви впевнені, що видалите цього користувача?</p>
            <form method="post">
                <input type="submit" name="submit" value="Delete" />
            </form>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>
