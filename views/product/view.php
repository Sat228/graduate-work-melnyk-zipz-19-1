<?php include ROOT . '/views/layouts/header.php'; ?>

<?php
use \App\models\Category;
use \App\models\Product;

?>

<!-- Main -->
<div id="mainBody">
    <div class="container">

        <div class="row">
            <!-- Sidebar -->
            <div id="sidebar" class="span3">
                <div class="well well-small">
                    <a id="myCart" href="/cart/">
                        <img src="/template/themes/images/ico-cart.png" alt="cart">
                        <span id="cart-count">
                        <?php echo $totalQuantity; ?>
                        </span> Товари у вашому кошику
                        <span class="badge badge-warning pull-right">
                        $<span id="cart-price"><?php echo $totalPrice; ?></span>
                     </span>
                    </a>
                </div>
                <ul id="sideManu" class="nav nav-tabs nav-stacked">
                    <?php foreach ($categories as $category): ?>
                        <li class="<?php if ($category['id'] == 1) echo 'subMenu open'; ?>">
                            <a href="/catalog/category-<?php echo $category['id'];?>">
                                <?php echo $category['name']; ?>
                            </a>
                            <?php if ($category['id'] == 1): ?>
                                <ul>
<!--                                    <Lorem Ipsum is simply dummy text.

li><a href="/catalog/category---><?php //echo $category['id'];?><!--">All</a></li>-->
                                    <?php if (isset($subCategories) && is_array($subCategories)): ?>
                                        <?php foreach ($subCategories as $subCategory): ?>
                                            <li>
                                                <a href="/catalog/category-<?php echo $category['id']."-".$subCategory['id'];?>">
                                                    <i class="icon-chevron-right"></i>
                                                    <?php echo $subCategory['name']; ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br/>
                <div class="thumbnail">
                    <img src="/template/themes/images/payment_methods.png" title="Bootshop Payment Methods" alt="Payments Methods">
                    <div class="caption">
                        <h5>методи оплати</h5>
                    </div>
                </div>
            </div>
            <!-- Sidebar end -->

            <div class="span9">
                <ul class="breadcrumb">
                    <li><a href="/">Головна</a> <span class="divider">/</span></li>
                    <li><a href="/catalog/category-9">Каталог</a> <span class="divider">/</span></li>
                    <li class="active">Деталі товару</li>
                </ul>
                <div class="row">
                    <div class="span3">
                        <div id="gallery">
                            <a href="<?php echo Product::getImage($product['id']); ?>">
                                <img src="<?php echo Product::getImage($product['id']); ?>" style="width:100%"/>
                            </a>
                        </div>
                    </div>
                    <div class="span6">
                        <h3><?php echo $product['name'];?> </h3>
                        <small><?php echo $product['title'];?></small>
                        <hr class="soft"/>
                        <form class="form-horizontal qtyFrm">
                            <div class="control-group">
                                <label class="control-label"><span>$<?php echo $product['price'];?></span></label>
                                <div class="controls">
                                    <a href="/cart/add/<?php echo $product['id']; ?>">
                                        <button type="submit" class="btn btn-large btn-primary pull-right add-to-cart" data-id="<?php echo $product['id'];?>" > Додати
                                            <i class=" icon-shopping-cart"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr class="soft"/>
                <h4>Інформація про Продукт</h4>
                <table class="table table-bordered">
                    <tbody>
                    <tr class="techSpecRow"><th colspan="2">Продукт деталі</th></tr>
                    <tr class="techSpecRow"><td class="techSpecTD1">Назва: </td><td class="techSpecTD2"><?php echo $product['name']; ?></td></tr>
                    <tr class="techSpecRow"><td class="techSpecTD1">Ціна:</td><td class="techSpecTD2"><?php echo $product['price'];?>$</td></tr>
                    <tr class="techSpecRow"><td class="techSpecTD1">Бренд:</td><td class="techSpecTD2"><?php echo $product['brand']; ?></td></tr>
                    <tr class="techSpecRow"><td class="techSpecTD1">Опис:</td><td class="techSpecTD2"> <?php echo $product['description']; ?></td></tr>
                    </tbody>
                </table>
                <div class="span9">
                    <ul id="productDetail" class="nav nav-tabs">
                        <li class="active"><a href="#profile" data-toggle="tab"><?php echo $totalFeaturedProducts;?> Рекомендовані товари</a></li>
                    </ul>
                    <div class="well well-small">
                        <div class="cycle-slideshow"
                             data-cycle-fx=carousel
                             data-cycle-timeout=5000
                             data-cycle-carousel-visible=3
                             data-cycle-carousel-fluid=true
                             data-cycle-slides="div.item"
                             data-cycle-prev="#prev"
                             data-cycle-next="#next"
                        >
                            <?php foreach ($sliderProducts as $sliderItem): ?>
                                <div class="item">
                                    <ul class="thumbnails">
                                        <li class="span3">
                                            <div class="thumbnail">
                                                <?php if ($sliderItem['is_new']): ?>
                                                    <i class="tag"></i>
                                                <?php endif; ?>
                                                <a href="/product/<?php echo $sliderItem['id'];?>">
                                                    <img class="product" src="<?php echo Product::getImage($sliderItem['id']); ?>" alt="">
                                                </a>
                                                <div class="caption">
                                                    <h5><?php echo $sliderItem['name'];?></h5>
                                                    <h4 style="text-align:center">
                                                        <a class="btn" href="/product/<?php echo $sliderItem['id'];?>">
                                                            <i class="icon-zoom-in"></i>
                                                        </a>
                                                        <a class="btn add-to-cart" data-id="<?php echo $sliderItem['id'];?>" href="#">В кошик
                                                            <i class="icon-shopping-cart"></i>
                                                        </a>
                                                        <a class="btn btn-primary" href="#">
                                                            $<?php echo $sliderItem['price'];?>
                                                        </a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            <?php endforeach; ?>
                            <a class="left carousel-control" id="prev" href="#featured" data-slide="prev">‹</a>
                            <a class="right carousel-control" id="next"  href="#featured" data-slide="next">›</a>
                        </div>
                    </div>
                    <br class="clr">
                </div>
            </div>
        </div>
</div>

<!-- MainBody End -->

<?php include ROOT . '/views/layouts/footer.php'; ?>

