<?php

namespace App\components;

/**
 * Клас Router
 * Компонент маршрутизації
 */
class Router {

    /**
     * Властивість для збереження масиву маршрутів
     * @var array
     */
    private $routes;

    /**
     * Конструктор
     */
    public function __construct()
    {
        $routesPath = ROOT . '/app/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Повертає рядок запиту
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Метод для обробки запиту
     */
    public function run()
    {
        // Отримуємо рядок запиту
        $uri = $this->getURI();

        // Перевіряємо наявність такого запиту в масиві маршрутів (routes.php)
        foreach ($this->routes as $uriPattern => $path) {

            // Порівнюємо $uriPattern та $uri
            if (preg_match("~$uriPattern~", $uri)) {

                // Отримуємо внутрішній шлях із зовнішнього відповідно до правила.
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                // Визначаємо контролер, дію, параметри
                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action'.ucfirst(array_shift($segments));

                $parameters = $segments;

                // Включаємо файл класу контролера
                $controllerFile = ROOT.'/app/controllers/'.$controllerName.'.php';
                if (is_file($controllerFile)) {
                    // Створюємо об'єкт, викликаємо метод (тобто дію)
                    $controllerName = sprintf("\App\controllers\%s", $controllerName);
                    $controllerObject = new $controllerName;
                }

                /* Викликаємо необхідний метод ($actionName) на конкретному
                 * класі ($controllerObject) з переданими параметрами ($parameters)
                 */
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                // Якщо метод контролера успішно викликано, зупиняємо роботу маршрутизатора
                if ($result != null) {
                    break;
                }
            }
        }
    }
}