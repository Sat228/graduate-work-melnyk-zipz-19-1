<?php

namespace App\models;
use App\components\Db;
use PDO;

class Order
{

    public static function save($userName, $userEmail, $userPhone, $userComment, $userId, $products)
    {
        // DB Connection
        $db = Db::getConnection();

        // DB query text
        $sql = "INSERT INTO product_order (user_name,user_email, user_phone, user_comment, user_id, products) "
            . "VALUES (:user_name, :user_email, :user_phone, :user_comment, :user_id, :products)";

        $products = json_encode($products);

        $result = $db->prepare($sql);
        $result->bindParam(":user_name", $userName, PDO::PARAM_STR);
        $result->bindParam(":user_email", $userEmail, PDO::PARAM_STR);
        $result->bindParam(":user_phone", $userPhone, PDO::PARAM_STR);
        $result->bindParam(":user_comment", $userComment, PDO::PARAM_STR);
        $result->bindParam(":user_id", $userId, PDO::PARAM_INT);
        $result->bindParam(":products", $products, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function updateOrderById($id, $options)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "UPDATE product_order SET "
            . "user_name = :userName,"
            . "user_email = :userEmail,"
            . "user_phone = :userPhone,"
            . "user_comment = :userComment,"
            . "user_id = :userId,"
            . "status = :status "
            . "WHERE id = :id";

        // Getting and returning the results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        $result->bindParam(":userName", $options['userName'], PDO::PARAM_STR);
        $result->bindParam(":userEmail", $options['userEmail'], PDO::PARAM_STR);
        $result->bindParam(":userPhone", $options['userPhone'], PDO::PARAM_STR);
        $result->bindParam(":userComment", $options['userComment'], PDO::PARAM_STR);
        $result->bindParam(":userId", $options['userId'], PDO::PARAM_INT);
        $result->bindParam(":status", $options['status'], PDO::PARAM_INT);
        $result->execute();
    }

    public static function getOrdersList()
    {
        // DB Connection
        $db = Db::getConnection();

        // DB query
        $sql = "SELECT id, user_email, user_name, date, status FROM product_order";

        // Getting and returning results
        $result = $db->query($sql);
        $ordersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_email'] = $row['user_email'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['status'] = $row['status'];
            $i++;
        }

        return $ordersList;
    }
    public static function getOrderListById($userId)
    {
        // Підключення до бази даних
        $db = Db::getConnection();

        // Запит до бази даних
        $sql = "SELECT id, user_email, user_name, date, status FROM product_order WHERE user_id = :userId";

        // Отримання результатів запиту
        $result = $db->prepare($sql);
        $result->bindParam(":userId", $userId, PDO::PARAM_INT);
        $result->execute();

        // Створення масиву з даними про замовлення
        $orders = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $orders[$i]['id'] = $row['id'];
            $orders[$i]['user_name'] = $row['user_name'];
            $orders[$i]['user_email'] = $row['user_email'];
            $orders[$i]['date'] = $row['date'];
            $orders[$i]['status'] = $row['status'];
            $i++;
        }

        return $orders;
    }

    public static function getOrderById($id)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query
        $sql = "SELECT * FROM product_order WHERE id = :id";

        // Get and returns results (use prepare request)
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Indicate that we want to get data in the form of an array
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Execute the request
        $result->execute();

        // Return data
        return $result->fetch();
    }

    public static function deleteOrderById($id)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query
        $sql = 'DELETE FROM product_order WHERE id = :id';

        // Getting and returning the results
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        $result->execute();
    }

    public static function getQuantityInOrder($id)
    {
        // Get information about order
        $order = self::getOrderById($id);

        $productsJson = $order['products'];
        $productsObject = json_decode($productsJson);
        $productsArray = [];

        foreach ($productsObject as $id => $quantity) {
            $productsArray[$id] = $quantity;
        }

        return $productsArray;
    }

    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Нове замовлення';
                break;
            case '2':
                return 'В обробці';
                break;
            case '3':
                return 'Доставлено';
                break;
            case '4':
                return 'Закрито';
                break;
        }
    }
}

