<?php

namespace App\models;
use App\components\Db;
use PDO;

class Search
{
    public static function searchData($searchTerm)
    {
        $db = Db::getConnection();
        $searchTerm = '%' . $searchTerm . '%';
        $sql = 'SELECT * FROM product WHERE name LIKE :searchTerm';

        $result = $db->prepare($sql);
        $result->bindParam(':searchTerm', $searchTerm);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $products = array();
        while ($row = $result->fetch()) {
            $products[] = $row;
        }

        return $products;
    }
}
