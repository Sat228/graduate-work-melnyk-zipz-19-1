<?php

namespace App\models;
use App\components\Db;
use PDO;


class User
{
    /**
     * User registration
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     * @param string $birth
     * @param string $company
     * @param string $address
     * @param string $city
     * @param string $state
     * @param integer $postcode
     * @param string $country
     * @param string $info
     * @param string $phone
     * @param string $role
     * @return boolean
     */


    public static function GetRoleAdm($email)
    {
        $db = Db::getConnection();

        // Запит до бази даних для отримання ролі користувача
        $sql = "SELECT role FROM users WHERE email = :email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        // Отримання результатів запиту та повернення ролі користувача
        $row = $result->fetch();
        if ($row) {
            return $row['role'];
        }
        return false;
    }
    public static function GetRoleModr($email)
    {
        $db = Db::getConnection();

        // Запит до бази даних для отримання ролі користувача
        $sql = "SELECT role FROM users WHERE email = :email";
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        // Отримання результатів запиту та повернення ролі користувача
        $row = $result->fetch();
        if ($row) {
            return $row['role'];
        }
        return false;
    }

    public static function register($firstName, $lastName, $email, $password, $birth, $company, $address, $city, $state, $postcode, $country, $info, $phone)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "INSERT INTO user (first_name, last_name, email, password, birth, company, "
            . "address, city, state, postcode, country, additional_info, phone) "
            . "VALUES (:firstName, :lastName, :email, :password, :birth, :company, "
            . ":address, :city, :state, :postcode, :country, :info, :phone)";

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":firstName", $firstName, PDO::PARAM_STR);
        $result->bindParam(":lastName", $lastName, PDO::PARAM_STR);
        $result->bindParam(":email", $email, PDO::PARAM_STR);
        $result->bindParam(":password", $password, PDO::PARAM_STR);
        $result->bindParam(":birth", $birth, PDO::PARAM_STR);
        $result->bindParam(":company", $company, PDO::PARAM_STR);
        $result->bindParam(":address", $address, PDO::PARAM_STR);
        $result->bindParam(":city", $city, PDO::PARAM_STR);
        $result->bindParam(":state", $state, PDO::PARAM_STR);
        $result->bindParam(":postcode", $postcode, PDO::PARAM_INT);
        $result->bindParam(":country", $country, PDO::PARAM_STR);
        $result->bindParam(":info", $info, PDO::PARAM_STR);
        $result->bindParam(":phone", $phone, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function update($id, $firstName, $lastName, $email, $password, $birth, $company, $address, $city, $state, $postcode, $country, $info, $phone)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'UPDATE user SET 
                first_name      = :firstName,
                last_name       = :lastName,
                email           = :email,
                password        = :password,
                birth           = :birth,
                company         = :company,
                address         = :address,
                city            = :city,
                state           = :state,
                postcode        = :postcode,
                country         = :country,
                additional_info = :info,
                phone = :phone WHERE id = :id,';

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        $result->bindParam(":firstName", $firstName, PDO::PARAM_STR);
        $result->bindParam(":lastName", $lastName, PDO::PARAM_STR);
        $result->bindParam(":email", $email, PDO::PARAM_STR);
        $result->bindParam(":password", $password, PDO::PARAM_STR);
        $result->bindParam(":birth", $birth, PDO::PARAM_STR);
        $result->bindParam(":company", $company, PDO::PARAM_STR);
        $result->bindParam(":address", $address, PDO::PARAM_STR);
        $result->bindParam(":city", $city, PDO::PARAM_STR);
        $result->bindParam(":state", $state, PDO::PARAM_STR);
        $result->bindParam(":postcode", $postcode, PDO::PARAM_INT);
        $result->bindParam(":country", $country, PDO::PARAM_STR);
        $result->bindParam(":info", $info, PDO::PARAM_STR);
        $result->bindParam(":phone", $phone, PDO::PARAM_STR);

        return $result->execute();
    }

    public static function checkUserData($email, $password)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'SELECT * FROM user WHERE email = :email AND password = :password';

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":email", $email, PDO::PARAM_STR);
        $result->bindParam(":password", $password, PDO::PARAM_STR);
        $result->execute();

        // Turn to the record
        $user = $result->fetch();

        if ($user) {
            // If the record exists, return user ID
            return $user['id'];
        }
        return false;
    }
    function getRoleByUsername($userName)
    {
        $db = Db::getConnection();
        // Підключення до бази даних

        // Виконання запиту до бази даних
        $query = "SELECT role FROM users WHERE username = '$userName'";
        $result = mysqli_query($db, $query);

        // Обробка результатів запиту
        if ($result && mysqli_fetch_assoc($result)) {
            mysqli_data_seek($result, 0);
            $row = mysqli_fetch_assoc($result);
            $role = $row['role'];
            return $role;
        } else {
            return false;
        }
    }

    public static function auth($userId)
    {
        // write the user ID in the session
        $_SESSION['user'] = $userId;
    }

    public static function checkLogged()
    {
        // If there is a session, return the user ID
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("Location: /login");
    }

    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    public static function getUserName()
    {
        // Get username by id
        if (isset($_SESSION['user'])) {
            $userId = $_SESSION['user'];
            $user = self::getUserById($userId);
            $userName = $user['first_name'];

            return $userName;
        }

        return 'guest';
    }

    public static function checkFirstName($firstName)
    {
        if (strlen($firstName) >= 2) {
            return true;
        }
        return false;
    }

    public static function checkLastName($lastName)
    {
        if (strlen($lastName) >= 2) {
            return true;
        }
        return false;
    }

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkEmailExists($email)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":email", $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    public static function checkPhone($phone)
    {
        if (strlen($phone) >= 10) {
            return true;
        }
        return false;
    }

    public static function getUserById($id)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "SELECT * FROM user WHERE id = :id";

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Indicate, that want to get data in the form of an array
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }
    public static function getUserByEmail($email)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "SELECT * FROM user WHERE email = :email";

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":email", $email, PDO::PARAM_STR);

        // Indicate, that want to get data in the form of an array
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }

    public static function resetPassword($email)
    {
        // DB connection
        $db = Db::getConnection();

        // New password
        $password = rand(123456, 987654);

        // DB query text
        $sql = "UPDATE user SET password = :password WHERE email = :email";

        // Getting and returning results. Prepare Request Used.
        $result = $db->prepare($sql);
        $result->bindParam(":email", $email, PDO::PARAM_STR);
        $result->bindParam(":password", $password, PDO::PARAM_INT);

        if ($result->execute()) {
            return $password;
        }
    }
    public static function getUserList()
    {
        // Підключення до БД
        $db = Db::getConnection();

        // Отримання та повернення результатів
        $result = $db->query('SELECT id, first_name, last_name, email, password, birth, company, address, city, state, postcode, country, additional_info, phone, date, status, role FROM user ORDER BY id ASC');
        $userList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $userList[$i]['id'] = $row['id'];
            $userList[$i]['first_name'] = $row['first_name'];
            $userList[$i]['last_name'] = $row['last_name'];
            $userList[$i]['email'] = $row['email'];
            $userList[$i]['password'] = $row['password'];
            $userList[$i]['birth'] = $row['birth'];
            $userList[$i]['company'] = $row['company'];
            $userList[$i]['address'] = $row['address'];
            $userList[$i]['city'] = $row['city'];
            $userList[$i]['state'] = $row['state'];
            $userList[$i]['postcode'] = $row['postcode'];
            $userList[$i]['country'] = $row['country'];
            $userList[$i]['additional_info'] = $row['additional_info'];
            $userList[$i]['phone'] = $row['phone'];
            $userList[$i]['date'] = $row['date'];
            $userList[$i]['status'] = $row['status'];
            $userList[$i]['role'] = $row['role'];
            $i++;
        }
        return $userList;
    }
    public static function deleteUserById($id)
    {
        // Підключення до БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'DELETE FROM user WHERE id = :id';

        // Отримання та повернення результату. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        return $result->execute();
    }
    public static function saveRole($role)
    {
        // DB Connection
        $db = Db::getConnection();

        // DB query text
        $sql = "INSERT INTO role) "
            . "VALUES (:role)";

        $result = $db->prepare($sql);
        $result->bindParam(":role", $role, PDO::PARAM_STR);

        return $result->execute();
    }
    public static function updateUserRoleById($id, $options)
    {
        // Підключення до БД
        $db = Db::getConnection();

        // Формування рядка запиту до БД
        $sql = "UPDATE user SET
            role = :role
            WHERE id = :id";

        // Підготовка запиту до БД
        $result = $db->prepare($sql);

        // Прив'язка значень параметрів до запиту
        $result->bindParam(":role", $options['role'], PDO::PARAM_STR);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Виконання запиту до БД
        return $result->execute();
    }
    public static function updateUserById($id, $options)
    {
        // Підключення до БД
        $db = Db::getConnection();

        // Формування рядка запиту до БД
        $sql = "UPDATE user SET
            first_name = :first_name,
            last_name = :last_name,
            email = :email,
            password = :password,
            birth = :birth,
            company = :company,
            address = :address,
            city = :city,
            state = :state,
            postcode = :postcode,
            country = :country,
            additional_info = :additional_info,
            phone = :phone,
            date = :date,
            status = :status,
            role = :role
            WHERE id = :id";

        // Підготовка запиту до БД
        $result = $db->prepare($sql);

        // Прив'язка значень параметрів до запиту
        $result->bindParam(":first_name", $options['first_name'], PDO::PARAM_STR);
        $result->bindParam(":last_name", $options['last_name'], PDO::PARAM_STR);
        $result->bindParam(":email", $options['email'], PDO::PARAM_STR);
        $result->bindParam(":password", $options['password'], PDO::PARAM_STR);
        $result->bindParam(":birth", $options['birth'], PDO::PARAM_STR);
        $result->bindParam(":company", $options['company'], PDO::PARAM_STR);
        $result->bindParam(":address", $options['address'], PDO::PARAM_STR);
        $result->bindParam(":city", $options['city'], PDO::PARAM_STR);
        $result->bindParam(":state", $options['state'], PDO::PARAM_STR);
        $result->bindParam(":postcode", $options['postcode'], PDO::PARAM_STR);
        $result->bindParam(":country", $options['country'], PDO::PARAM_STR);
        $result->bindParam(":additional_info", $options['additional_info'], PDO::PARAM_STR);
        $result->bindParam(":phone", $options['phone'], PDO::PARAM_STR);
        $result->bindParam(":date", $options['date'], PDO::PARAM_STR);
        $result->bindParam(":status", $options['status'], PDO::PARAM_STR);
        $result->bindParam(":role", $options['role'], PDO::PARAM_STR);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Виконання запиту до БД
        return $result->execute();
    }

}

