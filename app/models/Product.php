<?php

namespace App\models;
use App\components\Db;
use PDO;


class Product
{
    // Number of items displayed by default
    const SHOW_BY_DEFAULT = 6;

    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'SELECT id, name, price, is_new FROM product '
            . 'WHERE status="1" ORDER BY id DESC '
            . 'LIMIT :count';

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        // Indicate that we want to get data in the form of an array
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Command execution
        $result->execute();

        // Getting and returning results
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $productsList;
    }
    public static function getFeaturedProducts()
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'SELECT id, name, price, is_new FROM product '
            . 'WHERE status="1" AND is_featured="1" '
            . 'ORDER BY id DESC';

        $result = $db->query($sql);

        // Getting and returning results
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $productsList;
    }

    public static function getProductsListByCategory($categoryId, $page = 1)
    {
        $limit = Product::SHOW_BY_DEFAULT;
        // Offset(for request)
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "SELECT id, name, price, is_new, availability, title FROM product "
            . "WHERE status = '1' AND category_id = :category_id "
            . "ORDER BY id ASC LIMIT :limit OFFSET :offset";

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Command execution
        $result->execute();

        // Getting and returning results
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
            $products[$i]['title'] = $row['title'];
            $products[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $products;
    }

    public static function getProductsListBySubCategory($categoryId, $subCategoryId, $page = 1)
    {
        $limit = Product::SHOW_BY_DEFAULT;
        // Offset(for request)
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "SELECT id, name, price, is_new FROM product "
            . "WHERE status='1' AND category_id = :category_id AND sub_category_id = :sub_category_id "
            . "ORDER BY id ASC LIMIT :limit OFFSET :offset";

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);
        $result->bindParam(':sub_category_id', $subCategoryId, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Command execution
        $result->execute();

        // Getting and returning results
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
            $i++;
        }
        return $products;
    }
    public static function getProductById($id)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "SELECT * FROM product WHERE id = :id";

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);

        // Command execution
        $result->execute();

        // Getting and returning results
        return $result->fetch();
    }
    public static function getTotalProductsInCategory($categoryId)
    {
        // DB connection
        $db = Db::getConnection();

        // DB request text
        $sql = 'SELECT count(id) AS count FROM product WHERE status="1" AND category_id = :category_id';

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":category_id", $categoryId, PDO::PARAM_INT);

        // Command execution
        $result->execute();

        // Return value count - quantity
        $row = $result->fetch();
        return $row['count'];
    }
    public static function getTotalProductsInSubCategory($subcategoryId)
    {
        // DB connection
        $db = Db::getConnection();

        // DB request text
        $sql = 'SELECT count(id) AS count FROM product WHERE status="1" AND sub_category_id = :sub_category_id';

        // Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":sub_category_id", $subcategoryId, PDO::PARAM_INT);

        // Command execution
        $result->execute();

        // Return value count - quantity
        $row = $result->fetch();
        return $row['count'];
    }
    public static function getTotalProductsInFeaturedProducts()
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'SELECT count(id) AS count FROM product WHERE status="1" AND is_featured="1"';

        // Query execution
        $result = $db->query($sql);

        // Return value count - quantity
        $row = $result->fetch();
        return $row['count'];
    }
    public static function getProductsByIds($idsArray)
    {
        // DB connection
        $db = Db::getConnection();

        // Turn the array into a string to form a condition in the request
        $idsString = implode(',', $idsArray);

        // DB query text
        $sql = "SELECT * FROM product WHERE status = '1' AND id IN ($idsString)";

        $result = $db->query($sql);

        // Indicate, that we want to get data in the form of an array
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Getting and returning resultss
        $i = 0;
        $products = array();
        while($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['description'] = $row['description'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }
        return $products;
    }
    public static function getProductsList()
    {
        // DB connection
        $db = Db::getConnection();

        // Getting and returning results
        $result = $db->query('SELECT id, name, price, code FROM product ORDER BY id ASC');
        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['code'] = $row['code'];
            $i++;
        }
        return $productsList;
    }
    public static function deleteProductById($id)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'DELETE FROM product WHERE id = :id';

        // Getting and returning results. Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createProduct($options)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = 'INSERT INTO product '
            . '(name, code, price, category_id, sub_category_id, brand, availability, '
            . 'description, is_new, is_featured, status) '
            . 'VALUES '
            . '(:name, :code, :price, :category_id, :subcategory_id, :brand, :availability,'
            . ':description, :is_new, :is_featured, :status)';

        // Getting and returnring results. Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":name", $options['name'], PDO::PARAM_STR);
        $result->bindParam(":code", $options['code'], PDO::PARAM_STR);
        $result->bindParam(":price", $options['price'], PDO::PARAM_STR);
        $result->bindParam(":category_id", $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(":subcategory_id", $options['subcategory_id'], PDO::PARAM_INT);
        $result->bindParam(":brand", $options['brand'], PDO::PARAM_STR);
        $result->bindParam(":availability", $options['availability'], PDO::PARAM_INT);
        $result->bindParam(":description", $options['description'], PDO::PARAM_STR);
        $result->bindParam(":is_new", $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(":is_featured", $options['is_featured'], PDO::PARAM_INT);
        $result->bindParam(":status", $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            // If the request is completed successfully, return the id of the added record
            return $db->lastInsertId();
        }
        // Otherwise return 0
        return 0;
    }

    public static function updateProductById($id, $options)
    {
        // DB connection
        $db = Db::getConnection();

        // DB query text
        $sql = "UPDATE product
        SET
            name            = :name,
            code            = :code,
            price           = :price,
            category_id     = :category_id,
            sub_category_id = :subcategory_id,
            brand           = :brand,
            availability    = :availability,
            description     = :description,
            is_new          = :is_new,
            is_featured     = :is_featured,
            status          = :status
        WHERE id = :id";


        // Getting and returning results. Prepare Request Used
        $result = $db->prepare($sql);
        $result->bindParam(":id", $id, PDO::PARAM_INT);
        $result->bindParam(":name", $options['name'], PDO::PARAM_STR);
        $result->bindParam(":code", $options['code'], PDO::PARAM_STR);
        $result->bindParam(":price", $options['price'], PDO::PARAM_STR);
        $result->bindParam(":category_id", $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(":subcategory_id", $options['subcategory_id'], PDO::PARAM_INT);
        $result->bindParam(":brand", $options['brand'], PDO::PARAM_STR);
        $result->bindParam(":availability", $options['availability'], PDO::PARAM_INT);
        $result->bindParam(":description", $options['description'], PDO::PARAM_STR);
        $result->bindParam(":is_new", $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(":is_featured", $options['is_featured'], PDO::PARAM_INT);
        $result->bindParam(":status", $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }


    public static function getAvailabilityText($availability)
    {
        switch ($availability) {
            case '1':
                return 'In Stock';
                break;
            case '0':
                return 'Under the order';
                break;
        }
    }

    public static function getProductsListInOrder($id)
    {
        // Get information about order
        $order = Order::getOrderById($id);

        $productsJson = $order['products'];
        $productsObject = json_decode($productsJson);
        $productsArray = [];

        foreach ($productsObject as $id => $quantity) {
            $productsArray[$id] = $quantity;
        }

        $productsIds = array_keys($productsArray);
        $products = Product::getProductsByIds($productsIds);

        return $products;
    }

    public static function getImage($id)
    {
        // Dummy Image Name
        $noImage = 'no-image.jpg';

        // The path to the product folder
        $path = '/upload/images/products/';

        // The path to the product image
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            // If image for product exists
            // Return the product image path
            return $pathToProductImage;
        }

        // Return the dummy image path
        return $path . $noImage;
    }
    public static function getProductsBySearch($searchTerm)
    {
        // Підключення до бази даних
        $db = Db::getConnection();

        // Підготовка запиту
        $searchTerm = '%' . $searchTerm . '%';
        $sql = "SELECT * FROM product WHERE name LIKE :searchTerm";

        // Виконання запиту з використанням підготовленого запиту
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':searchTerm', $searchTerm, PDO::PARAM_STR);
        $stmt->execute();

        // Отримання результатів запиту
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $products;
    }
    public static function getProductsByCategory($categoryId, $subCategoryId, $searchTerm)
    {
        // Підключення до бази даних
        $db = Db::getConnection();

        // Підготовка запиту
        $searchTerm = '%' . $searchTerm . '%';
        $sql = "SELECT * FROM product WHERE category_id = :categoryId AND sub_category_id = :subCategoryId AND name LIKE :searchTerm";

        // Виконання запиту з використанням підготовленого запиту
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->bindParam(':subCategoryId', $subCategoryId, PDO::PARAM_INT);
        $stmt->bindParam(':searchTerm', $searchTerm, PDO::PARAM_STR);
        $stmt->execute();

        // Отримання результатів запиту
        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $products;
    }

    public static function getProductsBySubCategory($categoryId, $subCategoryId, $searchTerm, $page = 1)
    {
        $limit = Product::SHOW_BY_DEFAULT;
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();

        $searchTerm = '%' . $searchTerm . '%';
        $sql = "SELECT id, name, price, is_new FROM product "
            . "WHERE status='1' AND category_id = :categoryId AND sub_category_id = :subCategoryId "
            . "ORDER BY id ASC LIMIT :limit OFFSET :offset";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':categoryId', $categoryId, PDO::PARAM_INT);
        $stmt->bindParam(':subCategoryId', $subCategoryId, PDO::PARAM_INT);
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();

        $i = 0;
        $products = array();
        while ($row = $stmt->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['is_new'] = $row['is_new'];
            $i++;
        }

        return $products;
    }

}
