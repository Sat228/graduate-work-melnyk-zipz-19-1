<?php

namespace App\controllers;
use App\models\Category;
use App\models\Product;
use App\components\AdminBase;
use App\models\User;
use App\components\Cart;

/**
 * AdminProductController Controller
 * Product management in admin panel
 */
class AdminRoleController extends AdminBase
{
    /**
     * Action for the product management page
     */
    public function actionIndex()
    {

        // Get a list of products
        $usersList = User::getUserList();
        // Обробка форми
        // Chess access
        $userId = User::checkLogged();
        if ($userId == true) {
            $user = User::getUserById($userId);
            $role = $user['role'];
            $id = $user['id'];

        } else {
            // If there are not access then redirect in page login
            header ("Location: /login");
        }
        // Connect the view
        require_once(ROOT . '/views/admin_role/index.php');
        return true;
    }
    public function actionDelete($id)
    {
        // Access check
        self::checkAdmin();

        // Form processing
        if (isset($_POST['submit'])) {
            // If form is submitted
            // Delete the product
            User::deleteUserById($id);

            // Перенаправлення користувача на сторінку керування продуктом
            header("Location: /admin/role");
        }

        // Connect the view
        require_once(ROOT . '/views/admin_role/delete.php');
        return true;
    }


    public function actionUpdate($id)
    {
        // Перевірка доступу
        self::checkAdmin();

        // Отримання даних про конкретного користувача
        $user = User::getUserById($id);

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримання даних з форми
            $options = array(
                'role' => $_POST['role']
            );

            // Оновлення даних
            User::updateUserRoleById($id, $options);

            // Перенаправлення користувача на сторінку управління ролями
            header("Location: /admin/role");
        }

        // Підключення шаблону для відображення форми
        require_once(ROOT . '/views/admin_role/update.php');
        return true;
    }
}