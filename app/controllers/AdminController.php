<?php

namespace App\controllers;
use App\components\AdminBase;
use App\models\User;
use App\components\Cart;

/**
 * AdminController Controller
 * Home page in admin panel
 */
class AdminController extends AdminBase
{
    /**
         * Action for the admin panel start page
         */
    public function actionIndex()
    {
        // Access check
         self::checkAdmin();
        // Chess access
        $userId = User::checkLogged();
        if ($userId == true) {
            $user = User::getUserById($userId);
            $userName = $user['first_name'];
            $role = $user['role'];

        } else {
            // If there are not access then redirect in page login
            header ("Location: /login");
        }

        // Connect the view
        require_once(ROOT . '/views/admin/index.php');
        return true;
    }
}
