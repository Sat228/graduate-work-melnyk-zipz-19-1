<?php

namespace App\controllers;
use App\components\Db;
use App\models\Category;
use App\models\Product;
use App\components\Cart;
use App\components\AdminBase;

/**
 * ProductController
 * Product
 */
class ProductController
{
    /**
     *      * Action for product view page
     *      * @param integer $productId <p> product id </p>
     *      */
    public function actionView($productId)
    {
        // List of categories for the left menu
        $categories = Category::getCategoriesList();

        // List of subcategories for the left menu
        $subCategories = Category::getSubCategoriesList( 1 );

        // Get product info by id
        $product = Product::getProductById( $productId );

        $totalPrice = Cart::getPrice();
        $totalQuantity = Cart::countItems();

        // List of the featured products
        $sliderProducts = Product::getFeaturedProducts();
        // Count items in Featured Products
        $totalFeaturedProducts = Product::getTotalProductsInFeaturedProducts();


        // Отримати товари за пошуком
        $categoryProducts = [];
        if (!empty($searchTerm)) {
            $categoryProducts = Product::getProductsBySearch($searchTerm);
        }

        $totalPrice = Cart::getPrice();
        $totalQuantity = Cart::countItems();

        // Connect the view
        require_once(ROOT . '/views/product/view.php');
        return true;
    }
    public function actionCategory()
    {
        $categoryId = $_GET['categoryId']; // Отримати ID категорії з параметру 'categoryId' у URL
        $searchTerm = $_GET['searchTerm']; // Отримати рядок пошуку з параметру 'searchTerm' у URL

        // Отримати список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Отримати список підкатегорій для лівого меню
        $subCategories = Category::getSubCategoriesList($categoryId);

        // Розбити рядок пошуку на ідентифікатор підкатегорії та сам пошуковий термін
        $searchData = explode('-', $searchTerm);
        $subCategoryId = isset($searchData[1]) ? $searchData[1] : '';

        // Отримати товари для вибраної категорії з фільтрацією за підкатегорією та пошуковим терміном
        $categoryProducts = Product::getProductsByCategory($categoryId, $subCategoryId, $searchTerm);

        // Перевірити, чи потрібно змінити URL для врахування підкатегорії
        if ($subCategoryId && !$categoryProducts) {
            $url = "/catalog/category-$categoryId-$subCategoryId?searchTerm=$searchTerm";
            header("Location: $url");
            exit;
        }

        // Підключення відображення
        require_once(ROOT . '/views/catalog/category.php');
        return true;
    }

    public function actionSubcategory()
    {
        $categoryId = $_GET['categoryId']; // Отримати ідентифікатор категорії з параметра 'categoryId' у URL
        $searchTerm = $_GET['searchTerm']; // Отримати рядок пошуку з параметра 'searchTerm' у URL

        // Отримати список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Отримати список підкатегорій для лівого меню
        $subCategories = Category::getSubCategoriesList($categoryId);

        // Розбити рядок пошуку на ідентифікатор підкатегорії та сам пошуковий термін
        $searchData = explode('-', $searchTerm);
        $subCategoryId = '';
        if (count($searchData) > 1) {
            $subCategoryId = $searchData[0];
            $searchTerm = $searchData[1];
        } else {
            $searchTerm = $searchData[0];
        }

        // Формування URL-адреси з урахуванням підкатегорії
        $categoryUrl = "/catalog/category-$categoryId";
        $subCategoryUrl = "/catalog/category-$categoryId-$subCategoryId";
        $searchTerm = urlencode($searchTerm);
        $url = !empty($subCategoryId) ? "$subCategoryUrl?searchTerm=$searchTerm" : "$categoryUrl?searchTerm=$searchTerm";

        // Отримати товари для вибраної підкатегорії з фільтрацією за пошуковим терміном
        $categoryProducts = Product::getProductsBySubCategory($categoryId, $subCategoryId, $searchTerm);

        // Підключення вигляду
        require_once(ROOT . '/views/catalog/subcategory.php');
        return true;
    }






}

